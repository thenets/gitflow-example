FROM alpine

RUN apk add python3 py3-pip

WORKDIR /app

ADD requirements.txt .

RUN pip install -r requirements.txt

ADD ./src/ .

ENV FLASK_APP=main.py

CMD ["flask", "run", "--host=0.0.0.0"]
