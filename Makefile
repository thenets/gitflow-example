IMAGE_TAG=hello

build:
	docker build -t $(IMAGE_TAG) .

run:
	docker run -it --rm \
		-p 8080:5000 \
		$(IMAGE_TAG)

dev:
	docker run -it --rm \
		-p 8080:5000 \
		-v $(PWD)/src:/app \
		$(IMAGE_TAG)

build-run: build run
