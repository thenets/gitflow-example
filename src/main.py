from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, Cafe!'

@app.route('/contact')
def contact():
    return 'Ligue para 12345678 e mande email para kratos@asgard.com'
