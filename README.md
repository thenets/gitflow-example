01. Criar ambiente virtual para Python

```bash
virtualenv -p python3 venv
```

02. Criar arquivo de Hello World

Site simples usando Flask.

Docs: https://flask.palletsprojects.com/en/1.1.x/quickstart/

Crie um arquivo `src/main.py`:

```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'
```

03. Arquivo de dependências

Arquivo `requirements.txt`:

```requirements.txt
flask==1.1.2
```

04. Criar Dockerfile

```dockerfile
FROM alpine

RUN apk add python3 py3-pip

WORKDIR /app

ADD requirements.txt .

RUN pip install -r requirements.txt

ADD ./src/ .

ENV FLASK_APP=main.py

CMD ["flask", "run", "--host=0.0.0.0"]
```

```dockerignore
venv/
.vscode/
```

05. Makefile

Opcional, mas pode ajudar a automatizar processos.

```makefile
IMAGE_TAG=hello

build:
	docker build -t $(IMAGE_TAG) .

run:
	docker run -it --rm \
		-p 8080:5000 \
		$(IMAGE_TAG)

build-run: build run
```
